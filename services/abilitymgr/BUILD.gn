# Copyright (c) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//foundation/ability/ability_runtime/ability_runtime.gni")
import(
    "//foundation/ability/ability_runtime/services/abilitymgr/abilitymgr.gni")
import("//foundation/distributeddatamgr/relational_store/relational_store.gni")
import("//foundation/resourceschedule/background_task_mgr/bgtaskmgr.gni")

group("abilityms_target") {
  deps = [
    ":abilityms",
    ":ams_service_config",
  ]
}

config("abilityms_exception_config") {
  cflags_cc = [ "-fexceptions" ]
}

config("abilityms_config") {
  include_dirs = [
    "include/",
    "${ability_runtime_innerkits_path}/ability_manager/include",
    "${ability_runtime_innerkits_path}/app_manager/include",
    "${ability_runtime_innerkits_path}/connectionobs_manager/include",
    "${ability_base_path}/interfaces/inner_api/base/include",
    "${ability_base_kits_path}/extractortool/include",
    "${ability_base_kits_path}/uri/include",
    "${ability_base_kits_path}/want/include",
    "${ability_runtime_services_path}/common/include",
    "//prebuilts/jdk/jdk8/linux-x86/include",
    "//prebuilts/jdk/jdk8/linux-x86/include/linux",
    "//third_party/json/include",
    "${ability_runtime_path}/interfaces/kits/native/ability/native",
    "${relational_store_innerapi_path}/rdb/include",
    "${relational_store_innerapi_path}/appdatafwk/include",
    "${relational_store_innerapi_path}/dataability/include",
    "${ability_runtime_innerkits_path}/dataobs_manager/include",
    "${ability_runtime_path}/tools/aa/include",
    "//base/account/os_account/frameworks/common/account_error/include",
    "//base/account/os_account/frameworks/common/database/include",
    "//base/account/os_account/interfaces/innerkits/osaccount/native/include",
    "${ability_runtime_innerkits_path}/wantagent/include",
    "${ability_runtime_path}/interfaces/kits/native/appkit/ability_runtime",
    "${bgtaskmgr_interfaces_path}/innerkits/include",
    "${bgtaskmgr_frameworks_path}/common/include",
    "${bgtaskmgr_frameworks_path}/include",
    "${init_path}/interfaces/innerkits/include/syspara/",
    "${bundlefwk_inner_api_path}/appexecfwk_core/include",
  ]

  defines = []

  if (is_asan) {
    defines += [ "SUPPORT_ASAN" ]
  }

  if (ability_command_for_test) {
    defines += [ "ABILITY_COMMAND_FOR_TEST" ]
  }

  if (ability_runtime_graphics) {
    include_dirs += [
      "${graphic_path}/interfaces/inner_api",
      "${multimedia_path}/interfaces/innerkits/include",
      "${graphic_path}/interfaces/inner_api/wmservice",
      "${global_path}/i18n/frameworks/intl/include",
    ]

    defines += [ "SUPPORT_GRAPHICS" ]
  }

  if (background_task_mgr_continuous_task_enable) {
    defines += [ "BGTASKMGR_CONTINUOUS_TASK_ENABLE" ]
  }

  if (resource_schedule_service_enable) {
    defines += [ "RESOURCE_SCHEDULE_SERVICE_ENABLE" ]
  }

  if (efficiency_manager) {
    defines += [ "EFFICIENCY_MANAGER_ENABLE" ]
  }

  if (is_asan) {
    defines += [ "SUPPORT_ASAN" ]
  }

  cflags = []
  if (target_cpu == "arm") {
    cflags += [ "-DBINDER_IPC_32BIT" ]
  }
}

ohos_shared_library("abilityms") {
  shlib_type = "sa"
  sources = abilityms_files
  sources += [ "src/sa_mgr_client.cpp" ]
  cflags_cc = []
  configs = [
    ":abilityms_config",
    ":abilityms_exception_config",
  ]
  deps = [
    "${ability_runtime_innerkits_path}/uri_permission:uri_permission_mgr",
    "${ability_runtime_services_path}/common:event_report",
    "${ability_runtime_services_path}/common:perm_verification",
    "//third_party/icu/icu4c:shared_icuuc",
  ]

  external_deps = [
    "ability_base:configuration",
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:ability_deps_wrapper",
    "ability_runtime:ability_manager",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_manager",
    "ability_runtime:connection_obs_manager",
    "access_token:libaccesstoken_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "common_event_service:cesfwk_core",
    "common_event_service:cesfwk_innerkits",
    "dsoftbus:softbus_client",
    "eventhandler:libeventhandler",
    "hicollie_native:libhicollie",
    "hisysevent_native:libhisysevent",
    "hitrace_native:hitrace_meter",
    "hiviewdfx_hilog_native:libhilog",
    "init:libbeget_proxy",
    "init:libbegetutil",
    "ipc:ipc_core",
    "relational_store:native_dataability",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  if (background_task_mgr_continuous_task_enable) {
    external_deps += [ "background_task_mgr:bgtaskmgr_innerkits" ]
  }

  if (resource_schedule_service_enable) {
    external_deps += [ "resource_schedule_service:ressched_client" ]
  }

  if (efficiency_manager) {
    external_deps += [ "efficiency_manager:suspend_manager_client" ]
  }

  if (os_dlp_part_enabled) {
    cflags_cc += [ "-DWITH_DLP" ]
    external_deps += [ "dlp_permission_service:libdlp_permission_sdk" ]
  }

  if (ability_runtime_graphics) {
    deps += [
      "${ace_engine_path}/interfaces/inner_api/ui_service_manager:ui_service_mgr",
      "//third_party/icu/icu4c:shared_icuuc",
      "//third_party/libjpeg-turbo:turbojpeg_static",
    ]
    public_deps =
        [ "${global_path}/resource_management/frameworks/resmgr:global_resmgr" ]
    external_deps += [
      "i18n:intl_util",
      "input:libmmi-client",
      "multimedia_image_framework:image_native",
      "window_manager:libdm",
    ]
  }

  version_script = "libabilityms.map"
  subsystem_name = "ability"
  part_name = "ability_runtime"
}

ohos_prebuilt_etc("ams_service_config.json") {
  source = "resource/ams_service_config.json"
  subsystem_name = "ability"
  part_name = "ability_runtime"
}

group("ams_service_config") {
  deps = [ ":ams_service_config.json" ]
}
