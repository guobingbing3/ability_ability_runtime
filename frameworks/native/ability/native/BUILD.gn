# Copyright (c) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//foundation/ability/ability_runtime/ability_runtime.gni")
import("//foundation/distributeddatamgr/relational_store/relational_store.gni")

config("ability_config") {
  visibility = [ ":*" ]
  include_dirs = [
    "${ability_runtime_path}/interfaces/kits/native/ability/native",
    "${ability_runtime_path}/interfaces/kits/native/ability/native/continuation/distributed",
    "${ability_runtime_path}/interfaces/kits/native/ability/native/continuation/kits",
    "${ability_runtime_path}/interfaces/kits/native/ability/native/continuation/remote_register_service",
    "${ability_runtime_path}/interfaces/kits/native/ability/native/distributed_ability_runtime",
    "${ability_runtime_path}/interfaces/kits/native/appkit/app",
    "${ability_runtime_innerkits_path}/app_manager/include/appmgr",
    "${ability_runtime_innerkits_path}/uri/include",
    "${ability_runtime_services_path}/abilitymgr/include",
    "${ability_runtime_path}/interfaces/kits/native/appkit/ability_runtime/app",
    "${ability_runtime_path}/interfaces/kits/native/appkit/app",
    "${form_fwk_path}/interfaces/kits/native/include",
    "${ability_runtime_path}/interfaces/kits/native/appkit/app",
    "//third_party/node/src",
    "${ability_runtime_innerkits_path}/ability_manager/include/continuation",
    "${ability_runtime_path}/interfaces/kits/native/appkit/app/task",
    "${ability_runtime_napi_path}/inner/napi_common",
    "${ability_runtime_napi_path}/featureAbility",
  ]

  cflags = []
  if (target_cpu == "arm") {
    cflags += [ "-DBINDER_IPC_32BIT" ]
  }
  defines = [ "AMS_LOG_TAG = \"Ability\"" ]
  if (target_cpu == "arm64") {
    defines += [ "_ARM64_" ]
  }

  if (target_cpu == "arm64") {
    defines += [ "APP_USE_ARM64" ]
  } else if (target_cpu == "arm") {
    defines += [ "APP_USE_ARM" ]
  }

  if (ability_runtime_graphics) {
    include_dirs += [ "${form_fwk_path}/interfaces/inner_api/include" ]
    defines += [ "SUPPORT_GRAPHICS" ]
  }
}

config("ability_public_config") {
  visibility = [ ":*" ]
  include_dirs = [
    "${ability_runtime_napi_path}/inner/napi_ability_common",
    "${ability_runtime_path}/interfaces/kits/native/ability/native",
    "${ability_runtime_path}/interfaces/kits/native/ability/native/continuation/distributed",
    "${ability_runtime_path}/interfaces/kits/native/ability/native/continuation/kits",
    "${ability_runtime_path}/interfaces/kits/native/ability/native/continuation/remote_register_service",
    "${ability_runtime_path}/interfaces/kits/native/ability/native/distributed_ability_runtime",
    "${ability_runtime_path}/interfaces/kits/native/appkit",
    "${ability_runtime_path}/interfaces/kits/native/appkit/ability_runtime/app",
    "${ability_runtime_path}/interfaces/kits/native/appkit/app",
    "${ability_runtime_path}/interfaces/kits/native/appkit/ability_runtime",
    "${ability_runtime_path}/interfaces/kits/native/ability/native/recovery/",
    "//third_party/libuv/include",
    "${ability_runtime_path}/interfaces/kits/native/appkit/ability_runtime/context",
    "${ability_runtime_innerkits_path}/ability_manager/include/continuation",
    "${ability_runtime_services_path}/common/include",
    "//third_party/jsoncpp/include",
    "//third_party/json/include",
  ]

  if (ability_runtime_graphics) {
    include_dirs += [
      "${form_fwk_path}/interfaces/kits/native/include",
      "${form_fwk_path}/interfaces/inner_api/include",
      "${windowmanager_path}/interfaces/innerkits/wm",
      "${windowmanager_path}/interfaces/innerkits/dm",
      "${windowmanager_path}/interfaces/kits/napi/window_runtime/window_stage_napi",
      "${windowmanager_path}/utils/include",
    ]
    defines = [ "SUPPORT_GRAPHICS" ]
  }
}

ohos_shared_library("static_subscriber_ipc") {
  include_dirs =
      [ "${ability_runtime_path}/interfaces/kits/native/ability/native" ]

  sources = [
    "${ability_runtime_native_path}/ability/native/static_subscriber_proxy.cpp",
    "${ability_runtime_native_path}/ability/native/static_subscriber_stub.cpp",
  ]

  deps = []

  external_deps = [
    "ability_base:want",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "ipc:ipc_core",
  ]

  subsystem_name = "ability"
  part_name = "ability_runtime"
}

config("abilitykit_utils_public_config") {
  visibility = [ ":*" ]
  include_dirs = [
    "${ability_runtime_path}/interfaces/kits/native/ability/native/continuation/distributed",
    "${ability_runtime_path}/interfaces/kits/native/ability/native/continuation/kits",
    "${ability_runtime_path}/interfaces/kits/native/appkit/ability_runtime/context",
    "${ability_runtime_path}/interfaces/kits/native/appkit/ability_runtime",
    "${ability_runtime_path}/interfaces/kits/native/appkit/app",
    "${ability_runtime_innerkits_path}/app_manager/include/appmgr",
    "${bundlefwk_inner_api_path}/appexecfwk_base/include",
    "${bundlefwk_inner_api_path}/appexecfwk_core/include/bundlemgr",
    "//third_party/jsoncpp/include",
    "//third_party/json/include",
  ]

  if (ability_runtime_graphics) {
    include_dirs += [
      "${form_fwk_path}/interfaces/inner_api/include",
      "${multimedia_path}/interfaces/innerkits/include",
      "${windowmanager_path}/interfaces/innerkits/wm",
      "${windowmanager_path}/interfaces/innerkits/dm",
      "${windowmanager_path}/utils/include",
    ]
    defines = [ "SUPPORT_GRAPHICS" ]
  }
}

ohos_shared_library("abilitykit_utils") {
  include_dirs = [
    "${ability_runtime_path}/interfaces/kits/native/ability/native",
    "${ability_runtime_path}/interfaces/kits/native/appkit/ability_runtime/app",
    "${ability_runtime_innerkits_path}/ability_manager/include",
    "${ability_runtime_innerkits_path}/wantagent/include",
    "${ability_runtime_services_path}/abilitymgr/include",
    "${ability_runtime_services_path}/common/include",
  ]

  sources = [
    "${ability_runtime_native_path}/ability/native/ability_handler.cpp",
    "${ability_runtime_native_path}/ability/native/ability_local_record.cpp",
    "${ability_runtime_native_path}/ability/native/configuration_utils.cpp",
  ]

  deps = []

  if (ability_runtime_graphics) {
    deps += [ "//third_party/icu/icu4c:shared_icuuc" ]
  }

  public_configs = [ ":abilitykit_utils_public_config" ]

  external_deps = [
    "ability_base:configuration",
    "ability_base:want",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "eventhandler:libeventhandler",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_core",
    "ipc:ipc_napi_common",
    "napi:ace_napi",
    "resource_management:global_resmgr",
  ]

  subsystem_name = "ability"
  part_name = "ability_runtime"
}

ohos_shared_library("abilitykit_native") {
  include_dirs = [
    "${ability_base_kits_path}/extractortool/include",
    "${relational_store_napi_path}/rdb/include",
    "${relational_store_napi_path}/common/include",
    "${relational_store_napi_path}/dataability/include",
  ]

  sources = [
    "${ability_runtime_innerkits_path}/app_manager/src/appmgr/process_info.cpp",
    "${ability_runtime_native_path}/ability/native/ability.cpp",
    "${ability_runtime_native_path}/ability/native/ability_context.cpp",
    "${ability_runtime_native_path}/ability/native/ability_impl.cpp",
    "${ability_runtime_native_path}/ability/native/ability_impl_factory.cpp",
    "${ability_runtime_native_path}/ability/native/ability_lifecycle.cpp",
    "${ability_runtime_native_path}/ability/native/ability_lifecycle_executor.cpp",
    "${ability_runtime_native_path}/ability/native/ability_loader.cpp",
    "${ability_runtime_native_path}/ability/native/ability_post_event_timeout.cpp",
    "${ability_runtime_native_path}/ability/native/ability_process.cpp",
    "${ability_runtime_native_path}/ability/native/ability_runtime/js_ability.cpp",
    "${ability_runtime_native_path}/ability/native/ability_runtime/js_ability_context.cpp",
    "${ability_runtime_native_path}/ability/native/ability_runtime/js_caller_complex.cpp",
    "${ability_runtime_native_path}/ability/native/continuation/distributed/continuation_handler.cpp",
    "${ability_runtime_native_path}/ability/native/continuation/distributed/continuation_manager.cpp",
    "${ability_runtime_native_path}/ability/native/continuation/distributed/reverse_continuation_scheduler_primary.cpp",
    "${ability_runtime_native_path}/ability/native/data_ability_helper_impl.cpp",
    "${ability_runtime_native_path}/ability/native/data_ability_impl.cpp",
    "${ability_runtime_native_path}/ability/native/data_uri_utils.cpp",
    "${ability_runtime_native_path}/ability/native/distributed_ability_runtime/distributed_client.cpp",
    "${ability_runtime_native_path}/ability/native/extension.cpp",
    "${ability_runtime_native_path}/ability/native/extension_config_mgr.cpp",
    "${ability_runtime_native_path}/ability/native/extension_impl.cpp",
    "${ability_runtime_native_path}/ability/native/extension_module_loader.cpp",
    "${ability_runtime_native_path}/ability/native/free_install_observer_proxy.cpp",
    "${ability_runtime_native_path}/ability/native/free_install_observer_stub.cpp",
    "${ability_runtime_native_path}/ability/native/js_extension_common.cpp",
    "${ability_runtime_native_path}/ability/native/js_free_install_observer.cpp",
    "${ability_runtime_native_path}/ability/native/new_ability_impl.cpp",
    "${ability_runtime_native_path}/ability/native/recovery/ability_recovery.cpp",
    "${ability_runtime_native_path}/ability/native/recovery/app_recovery.cpp",
    "${ability_runtime_native_path}/ability/native/service_ability_impl.cpp",
    "${ability_runtime_native_path}/appkit/ability_runtime/extension_context.cpp",
    "${ability_runtime_native_path}/appkit/app/app_context.cpp",
    "${ability_runtime_native_path}/appkit/app/context_container.cpp",
    "${ability_runtime_native_path}/appkit/app/context_deal.cpp",
    "${ability_runtime_native_path}/appkit/app/sys_mgr_client.cpp",
    "${ability_runtime_services_path}/abilitymgr/src/ability_start_setting.cpp",
    "${ability_runtime_services_path}/abilitymgr/src/launch_param.cpp",
  ]
  configs = [ ":ability_config" ]
  public_configs = [
    ":ability_public_config",
    "${ability_runtime_native_path}/ability:ability_context_public_config",
    "${ability_runtime_innerkits_path}/wantagent:wantagent_innerkits_public_config",
  ]

  deps = [
    ":continuation_ipc",
    ":extension_blocklist_config",
    "${ability_runtime_innerkits_path}/dataobs_manager:dataobs_manager",
    "${ability_runtime_native_path}/ability/native:ability_business_error",
    "${ability_runtime_native_path}/appkit:app_context",
    "${ability_runtime_native_path}/appkit:app_context_utils",
    "${ability_runtime_native_path}/appkit:appkit_delegator",
    "${ability_runtime_services_path}/common:event_report",
  ]

  external_deps = [
    "ability_base:base",
    "ability_base:configuration",
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:ability_context_native",
    "ability_runtime:ability_deps_wrapper",
    "ability_runtime:ability_manager",
    "ability_runtime:napi_base_context",
    "ability_runtime:runtime",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "access_token:libtokenid_sdk",
    "bundle_framework:appexecfwk_base",
    "c_utils:utils",
    "hisysevent_native:libhisysevent",
    "hitrace_native:hitrace_meter",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_core",
    "ipc:ipc_napi_common",
    "relational_store:native_rdb",
    "samgr:samgr_proxy",
  ]

  defines = []

  if (background_task_mgr_continuous_task_enable) {
    external_deps += [ "background_task_mgr:bgtaskmgr_innerkits" ]
    defines += [ "BGTASKMGR_CONTINUOUS_TASK_ENABLE" ]
  }

  if (efficiency_manager) {
    external_deps += [ "efficiency_manager:suspend_manager_client" ]
    defines += [ "EFFICIENCY_MANAGER_ENABLE" ]
  }

  public_deps = [
    ":abilitykit_utils",
    "${ability_runtime_napi_path}/inner/napi_common:napi_common",
    "//base/notification/eventhandler/frameworks/eventhandler:libeventhandler",
    "//base/startup/init/interfaces/innerkits:libbegetutil",
    "//foundation/arkui/napi:ace_napi",
    "//foundation/communication/ipc/interfaces/kits/js/napi:rpc",
  ]

  if (ability_runtime_graphics) {
    include_dirs += []
    sources += [
      "${ability_runtime_native_path}/ability/native/ability_window.cpp",
      "${ability_runtime_native_path}/ability/native/form_provider_client.cpp",
      "${ability_runtime_native_path}/ability/native/page_ability_impl.cpp",
    ]
    deps += [
      "${multimodalinput_path}/frameworks/proxy:libmmi-common",
      "//third_party/icu/icu4c:shared_icuuc",
    ]

    external_deps += [
      "form_fwk:fmskit_native",
      "form_fwk:form_manager",
      "multimedia_image_framework:image",
    ]

    public_deps += [
      "${multimedia_path}/interfaces/innerkits:image_native",
      "${windowmanager_path}/dm:libdm",
      "${windowmanager_path}/interfaces/kits/napi/window_runtime:windowstage_kit",
      "${windowmanager_path}/wm:libwm",
    ]
  }

  version_script = "libabilitykit_native.map"
  subsystem_name = "ability"
  part_name = "ability_runtime"
}

config("extensionkit_public_config") {
  visibility = [ ":*" ]
  include_dirs = [
    "${ability_runtime_path}/interfaces/kits/native/ability/native/continuation/distributed",
    "${ability_runtime_path}/interfaces/kits/native/ability/native/continuation/kits",
    "${ability_runtime_path}/interfaces/kits/native/appkit/ability_runtime/context",
    "${ability_runtime_path}/interfaces/kits/native/appkit/ability_runtime",
    "${ability_runtime_path}/interfaces/kits/native/appkit/app",
    "${ability_runtime_innerkits_path}/app_manager/include/appmgr",
    "${bundlefwk_inner_api_path}/appexecfwk_base/include",
    "${bundlefwk_inner_api_path}/appexecfwk_core/include/bundlemgr",
    "//third_party/jsoncpp/include",
    "//third_party/json/include",
  ]

  if (ability_runtime_graphics) {
    include_dirs += [
      "${form_fwk_path}/interfaces/inner_api/include",
      "${multimedia_path}/interfaces/innerkits/include",
      "${windowmanager_path}/interfaces/innerkits/wm",
      "${windowmanager_path}/interfaces/innerkits/dm",
      "${windowmanager_path}/utils/include",
    ]
    defines = [ "SUPPORT_GRAPHICS" ]
  }
}

ohos_shared_library("extensionkit_native") {
  include_dirs = [
    "${ability_runtime_path}/interfaces/kits/native/ability/native",
    "${ability_runtime_path}/interfaces/kits/native/appkit/ability_runtime",
    "${ability_runtime_napi_path}/inner/napi_common",
  ]

  sources = [
    "${ability_runtime_native_path}/ability/native/extension.cpp",
    "${ability_runtime_native_path}/ability/native/extension_config_mgr.cpp",
    "${ability_runtime_native_path}/ability/native/extension_impl.cpp",
    "${ability_runtime_native_path}/ability/native/extension_module_loader.cpp",
    "${ability_runtime_native_path}/ability/native/js_extension_common.cpp",
    "${ability_runtime_native_path}/appkit/ability_runtime/extension_context.cpp",
  ]

  public_configs = [ ":extensionkit_public_config" ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:ability_context_native",
    "ability_runtime:ability_manager",
    "ability_runtime:napi_common",
    "ability_runtime:runtime",
    "c_utils:utils",
    "hitrace_native:hitrace_meter",
    "hiviewdfx_hilog_native:libhilog",
  ]

  public_deps = [
    ":abilitykit_utils",
    "${arkui_path}/napi:ace_napi",
    "${eventhandler_path}/frameworks/eventhandler:libeventhandler",
  ]

  subsystem_name = "ability"
  part_name = "ability_runtime"
}

ohos_shared_library("ability_thread") {
  include_dirs =
      [ "${ability_runtime_path}/interfaces/kits/native/ability/native" ]

  sources =
      [ "${ability_runtime_native_path}/ability/native/ability_thread.cpp" ]

  deps = [
    ":abilitykit_native",
    "${ability_runtime_innerkits_path}/dataobs_manager:dataobs_manager",
    "${ability_runtime_native_path}/appkit:app_context",
  ]

  external_deps = [
    "ability_base:configuration",
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:ability_context_native",
    "ability_runtime:ability_manager",
    "c_utils:utils",
    "hitrace_native:hitrace_meter",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_core",
  ]

  subsystem_name = "ability"
  part_name = "ability_runtime"
}

ohos_shared_library("form_extension") {
  if (ability_runtime_graphics) {
    include_dirs = [
      "${ability_runtime_path}/interfaces/kits/native/ability/native",
      "${ability_runtime_path}/interfaces/kits/native/appkit/ability_runtime",
    ]

    sources = [
      "${ability_runtime_native_path}/ability/native/form_extension.cpp",
      "${ability_runtime_native_path}/ability/native/form_js_event_handler.cpp",
      "${ability_runtime_native_path}/ability/native/form_runtime/form_extension_provider_client.cpp",
      "${ability_runtime_native_path}/ability/native/form_runtime/js_form_extension.cpp",
      "${ability_runtime_native_path}/ability/native/form_runtime/js_form_extension_context.cpp",
      "${ability_runtime_native_path}/appkit/ability_runtime/form_extension_context.cpp",
    ]

    deps = [
      ":abilitykit_native",
      "${ability_runtime_napi_path}/inner/napi_common:napi_common",
      "${ability_runtime_native_path}/appkit:app_context",
      "${form_fwk_napi_path}:formutil_napi",
    ]

    external_deps = [
      "ability_base:want",
      "ability_runtime:ability_context_native",
      "ability_runtime:runtime",
      "c_utils:utils",
      "form_fwk:fmskit_native",
      "form_fwk:form_manager",
      "hiviewdfx_hilog_native:libhilog",
      "ipc:ipc_core",
    ]
  }
  subsystem_name = "ability"
  part_name = "ability_runtime"
}

ohos_shared_library("form_extension_module") {
  include_dirs =
      [ "${ability_runtime_path}/interfaces/kits/native/ability/native" ]

  sources = [ "${ability_runtime_native_path}/ability/native/form_extension_module_loader.cpp" ]

  configs = [ ":ability_config" ]
  public_configs = [ ":ability_public_config" ]

  deps = [ ":form_extension" ]

  external_deps = [
    "ability_base:configuration",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_core",
    "napi:ace_napi",
  ]

  relative_install_dir = "extensionability"
  subsystem_name = "ability"
  part_name = "ability_runtime"
}

ohos_shared_library("service_extension") {
  include_dirs = [
    "${ability_runtime_path}/interfaces/kits/native/ability/native",
    "${ability_runtime_path}/interfaces/kits/native/appkit/ability_runtime",
  ]

  sources = [
    "${ability_runtime_native_path}/ability/native/js_service_extension.cpp",
    "${ability_runtime_native_path}/ability/native/js_service_extension_context.cpp",
    "${ability_runtime_native_path}/ability/native/service_extension.cpp",
    "${ability_runtime_native_path}/appkit/ability_runtime/service_extension_context.cpp",
  ]

  deps = [
    ":abilitykit_native",
    "${ability_runtime_napi_path}/inner/napi_common:napi_common",
    "${ability_runtime_native_path}/ability/native:ability_business_error",
    "${ability_runtime_native_path}/appkit:app_context",
  ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:ability_context_native",
    "ability_runtime:ability_manager",
    "ability_runtime:runtime",
    "c_utils:utils",
    "eventhandler:libeventhandler",
    "hitrace_native:hitrace_meter",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_core",
    "ipc:ipc_napi_common",
    "napi:ace_napi",
  ]

  subsystem_name = "ability"
  part_name = "ability_runtime"
}

ohos_shared_library("static_subscriber_extension") {
  include_dirs = [
    "${ability_runtime_path}/interfaces/kits/native/ability/native",
    "${ability_runtime_path}/interfaces/kits/native/appkit/ability_runtime",
  ]

  sources = [
    "${ability_runtime_native_path}/ability/native/js_static_subscriber_extension.cpp",
    "${ability_runtime_native_path}/ability/native/js_static_subscriber_extension_context.cpp",
    "${ability_runtime_native_path}/ability/native/static_subscriber_extension.cpp",
    "${ability_runtime_native_path}/ability/native/static_subscriber_stub_imp.cpp",
    "${ability_runtime_native_path}/appkit/ability_runtime/static_subscriber_extension_context.cpp",
  ]

  deps = [
    ":extensionkit_native",
    ":static_subscriber_ipc",
    "${ability_runtime_napi_path}/inner/napi_common:napi_common",
    "${ability_runtime_native_path}/appkit:app_context",
  ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:ability_context_native",
    "ability_runtime:runtime",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "eventhandler:libeventhandler",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_core",
    "napi:ace_napi",
  ]

  subsystem_name = "ability"
  part_name = "ability_runtime"
}

ohos_shared_library("static_subscriber_extension_module") {
  include_dirs =
      [ "${ability_runtime_path}/interfaces/kits/native/ability/native" ]

  sources = [ "${ability_runtime_native_path}/ability/native/static_subscriber_extension_module_loader.cpp" ]

  configs = [ ":ability_config" ]
  public_configs = [ ":ability_public_config" ]

  deps = [ ":static_subscriber_extension" ]

  external_deps = [
    "ability_base:configuration",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_core",
    "napi:ace_napi",
  ]

  relative_install_dir = "extensionability"
  subsystem_name = "ability"
  part_name = "ability_runtime"
}

ohos_shared_library("continuation_ipc") {
  include_dirs = []

  sources = [
    "${ability_runtime_native_path}/ability/native/continuation/distributed/reverse_continuation_scheduler_primary_proxy.cpp",
    "${ability_runtime_native_path}/ability/native/continuation/distributed/reverse_continuation_scheduler_primary_stub.cpp",
    "${ability_runtime_native_path}/ability/native/continuation/distributed/reverse_continuation_scheduler_recipient.cpp",
    "${ability_runtime_native_path}/ability/native/continuation/distributed/reverse_continuation_scheduler_replica.cpp",
    "${ability_runtime_native_path}/ability/native/continuation/distributed/reverse_continuation_scheduler_replica_proxy.cpp",
    "${ability_runtime_native_path}/ability/native/continuation/distributed/reverse_continuation_scheduler_replica_stub.cpp",
    "${ability_runtime_native_path}/ability/native/continuation/remote_register_service/connect_callback_proxy.cpp",
    "${ability_runtime_native_path}/ability/native/continuation/remote_register_service/connect_callback_stub.cpp",
    "${ability_runtime_native_path}/ability/native/continuation/remote_register_service/continuation_connector.cpp",
    "${ability_runtime_native_path}/ability/native/continuation/remote_register_service/continuation_device_callback_proxy.cpp",
    "${ability_runtime_native_path}/ability/native/continuation/remote_register_service/continuation_register_manager.cpp",
    "${ability_runtime_native_path}/ability/native/continuation/remote_register_service/continuation_register_manager_proxy.cpp",
    "${ability_runtime_native_path}/ability/native/continuation/remote_register_service/remote_register_service_proxy.cpp",
    "${ability_runtime_native_path}/ability/native/continuation/remote_register_service/remote_register_service_stub.cpp",
  ]

  configs = [ ":ability_config" ]

  public_configs = [ ":ability_public_config" ]

  deps = [ ":abilitykit_utils" ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:ability_manager",
    "c_utils:utils",
    "eventhandler:libeventhandler",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_core",
    "resource_management:global_resmgr",
  ]

  defines = []

  if (background_task_mgr_continuous_task_enable) {
    external_deps += [ "background_task_mgr:bgtaskmgr_innerkits" ]
    defines += [ "BGTASKMGR_CONTINUOUS_TASK_ENABLE" ]
  }

  public_deps = []

  subsystem_name = "ability"
  part_name = "ability_runtime"
}

ohos_shared_library("data_ability_helper") {
  include_dirs =
      [ "${ability_runtime_path}/interfaces/kits/native/ability/native" ]

  sources = [
    "${ability_runtime_native_path}/ability/native/data_ability_helper.cpp",
  ]

  configs = [ ":ability_config" ]
  public_configs = [ ":ability_public_config" ]

  deps = [ ":abilitykit_native" ]

  external_deps = [
    "ability_base:zuri",
    "c_utils:utils",
    "data_share:datashare_common",
    "data_share:datashare_consumer",
    "hitrace_native:hitrace_meter",
    "hiviewdfx_hilog_native:libhilog",
    "relational_store:native_dataability",
    "relational_store:native_rdb",
    "relational_store:rdb_data_ability_adapter",
  ]

  subsystem_name = "ability"
  part_name = "ability_runtime"
}

ohos_shared_library("service_extension_module") {
  include_dirs =
      [ "${ability_runtime_path}/interfaces/kits/native/ability/native" ]

  sources = [ "${ability_runtime_native_path}/ability/native/service_extension_module_loader.cpp" ]

  configs = [ ":ability_config" ]
  public_configs = [ ":ability_public_config" ]

  deps = [ ":service_extension" ]

  external_deps = [
    "ability_base:configuration",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_core",
    "napi:ace_napi",
  ]

  relative_install_dir = "extensionability/"
  subsystem_name = "ability"
  part_name = "ability_runtime"
}

config("ability_business_error_config") {
  visibility = [ ":*" ]
  include_dirs = [
    "${ability_runtime_innerkits_path}/ability_manager/include",
    "${ability_runtime_path}/interfaces/kits/native/ability/native/ability_business_error",
    "//base/hiviewdfx/hiview/adapter/utility/include/extra",
  ]
}

config("ability_business_error_public_config") {
  visibility = [ ":*" ]
  include_dirs = [ "${ability_runtime_path}/interfaces/kits/native/ability/native/ability_business_error" ]
}

ohos_shared_library("ability_business_error") {
  sources = [ "${ability_runtime_native_path}/ability/native/ability_business_error/ability_business_error.cpp" ]

  configs = [ ":ability_business_error_config" ]

  public_configs = [ ":ability_business_error_public_config" ]

  subsystem_name = "ability"
  part_name = "ability_runtime"
}

config("dialog_request_callback_config") {
  visibility = [ ":*" ]
  include_dirs = [ "${ability_runtime_path}/interfaces/kits/native/ability/native/dialog_request_callback" ]
}

config("dialog_request_callback_public_config") {
  visibility = [ ":*" ]
  include_dirs = [ "${ability_runtime_path}/interfaces/kits/native/ability/native/dialog_request_callback" ]
}

ohos_shared_library("dialog_request_callback") {
  sources = [
    "${ability_runtime_native_path}/ability/native/dialog_request_callback/dialog_request_callback_proxy.cpp",
    "${ability_runtime_native_path}/ability/native/dialog_request_callback/dialog_request_callback_stub.cpp",
  ]

  configs = [
    ":dialog_request_callback_config",
    "${ability_runtime_services_path}/common:common_config",
  ]

  public_configs = [ ":dialog_request_callback_public_config" ]

  external_deps = [
    "c_utils:utils",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_core",
  ]

  subsystem_name = "ability"
  part_name = "ability_runtime"
}

ohos_prebuilt_etc("extension_blocklist_config.json") {
  source = "etc/extension_blocklist_config.json"
  subsystem_name = "ability"
  part_name = "ability_runtime"
}

group("extension_blocklist_config") {
  deps = [ ":extension_blocklist_config.json" ]
}

group("extension_module") {
  deps = [
    ":service_extension_module",
    ":static_subscriber_extension_module",
  ]

  if (ability_runtime_graphics) {
    deps += [ ":form_extension_module" ]
  }
}
